import React, { Component } from 'react';
import Header from './header';
import Jumbo from './jumbo';
import Footer from './footer';
import ImageComponent from './image_component';
import AnimatedElement from './animated_element';

import BannerImg from '../../assets/images/banner.jpg'; 
import IconShipping from '../../assets/images/icon_shipping.png';
import IconBuyer from '../../assets/images/icon_buyer.png';
import TestimonialProfile from '../../assets/images/Testimonial.jpg';
import ScreensImage from '../../assets/images/Screens.png';
import BannerI from '../../assets/images/instantly.jpg';
import GroceWheels from '../../assets/images/groce_wheels.png';
import HuzaBook from '../../assets/images/huza_book.png';
import DokmaiRwanda from '../../assets/images/dokmai_rwanda.png';
import ShemaShop from '../../assets/images/shema_shop.png';
import Bepartner from '../../assets/images/you.jpg';


class LandingPage extends Component {

    constructor(props) {
        super(props);
    }

    getBannerImageRatio(width, height) {
        // this could be done on the server side
        // doing this for the sake of the test
        var ratio = (height / width) * 100;
        return ratio;
    }

    render() {
        const bannerInlineStyles = {
            paddingTop: this.getBannerImageRatio(1280, 400) + "%"
        }
        const screenImageContainerInlineStyles = {
            paddingTop: this.getBannerImageRatio(1280, 400) + "%"
        }
        const banner2InlineStyles = {
            paddingTop: this.getBannerImageRatio(1280, 500) + "%"
        }
        const banner3InlineStyles = {
            paddingTop: this.getBannerImageRatio(1280, 500) + "%"
        }
        return (
            <div>
                <Header />
                <div className="row banner-container" style={bannerInlineStyles}>
                    <ImageComponent src={BannerImg}/>
                </div>
                <div className="row Hero-container">
                    <div className="col-6 col-lg-6"> 
                        <div className="row hero-context-container shipping-container">
                            <div className="hero-image-container">
                                <img className="hero-logo" src={IconShipping} />
                            </div>
                            
                            <h5 className="hero-subject">Shippings</h5>
                            <h2 className="hero-statistics">
                                3,456,230
                            </h2>
                        </div> 
                    </div>
                    <div className="col-6 col-lg-6"> 
                        <div className="row hero-context-container buyer-container">
                            <div className="hero-image-container">
                                <img className="hero-logo" src={IconBuyer} />
                            </div>
                            <h5 className="hero-subject">Buyers</h5>
                            <h2 className="hero-statistics">
                                1,140,000
                            </h2>
                        </div> 
                    </div>               
                </div>
                <AnimatedElement>
                    <div className="row testimonials-container">
                            <h5 className="row-title">
                                They loved it
                            </h5>
                            <div className="col-lg-6">
                                <div className="row inner left">
                                    <div className="inner-element testifier-profile-image">
                                        <ImageComponent src={TestimonialProfile} />
                                    </div>
                                    <h5 className="inner-element testifier-name">
                                        Ritah <em>Uwera</em>
                                    </h5>
                                    <h5 className="inner-element testifier-role">
                                        MANAGING DIRECTOR - TODDLE CARE
                                    </h5>
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="row inner right">
                                    <p>
                                    “I couldn’t believe how affordable the service was, given the value they were selling ... I decided to work with them to
                                    have my business online. I’ve seen my sales grow tremendously thanks to the quality of their services and a constant
                                    support to my growth.”
                                    </p>
                                </div>
                            </div>
                    </div>
                </AnimatedElement>
                <AnimatedElement>
                <div className="layout container">
                    <div className="row get-container">
                        <h5 className="row-title">
                            What you get
                        </h5>
                        <div className="col-lg-12">
                            <div className="row image-representer-container" style={screenImageContainerInlineStyles}>
                                <ImageComponent src={ScreensImage}/>
                            </div>
                        </div>
                        <div className="col-lg-12">
                            <div className="row representer-btn-container">
                                <div className="col-md-6">
                                    <a href="javascript:void(0)">
                                        Store front & Inventory
                                    </a>
                                </div>
                                <div className="col-md-6">
                                    <a href="javascript:void(0)">
                                        Shipping & pickup
                                    </a>
                                </div>
                            </div>
                            <div className="row representer-btn-container">
                                <div className="col-md-6">
                                    <a href="javascript:void(0)">
                                        Customer Management
                                    </a>
                                </div>
                                <div className="col-md-6">
                                    <a href="javascript:void(0)">
                                        Marketing
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </AnimatedElement>
                <AnimatedElement>
                <Jumbo image={BannerI} addc="get_ban" styles={banner2InlineStyles}>
                    <h5>Get paid instantly</h5>
                </Jumbo>
                </AnimatedElement>
                <AnimatedElement>
                <div className="layout container">
                    <div className="row partners-container">
                        <h5 className="title">
                            They are online
                        </h5>
                        <div className="col-lg-12">
                            <div className="row logo-container">
                                <div className="col-6 col-md-3">
                                    <a href="javascript:void(0)">
                                        <ImageComponent src={GroceWheels}/>
                                    </a>
                                </div>
                                <div className="col-6 col-md-3">
                                    <a href="javascript:void(0)">
                                        <ImageComponent src={HuzaBook} />
                                    </a>
                                </div>
                                <div className="col-6 col-md-3">
                                    <a href="javascript:void(0)">
                                        <ImageComponent src={DokmaiRwanda} />
                                    </a>
                                </div>
                                <div className="col-6 col-md-3">
                                    <a href="javascript:void(0)">
                                        <ImageComponent src={ShemaShop} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </AnimatedElement>
                <AnimatedElement>
                <Jumbo image={Bepartner} addc="could_be" styles={banner3InlineStyles}>
                    <h5>This could be you</h5>
                    <a href="javascript:void(0)" className="start-btn">Start Now</a>
                </Jumbo>
                </AnimatedElement>
                <Footer />
            </div>
        )
    }
}

export default LandingPage;