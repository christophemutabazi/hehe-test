import React  from 'react';
import ImageComponent from './image_component';

const Jumbo = (props) => {
    const className = (props.addc !== "") ? ("image-c " + props.addc) :"image-c";
    return(
        <div className={className} style={props.styles}>
            <ImageComponent src={props.image}/>
            <div className="text-container">
                {props.children}
            </div>
        </div>
    )
}

export default Jumbo;