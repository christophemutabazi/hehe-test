import React from 'react';
import HeadRoom from 'react-headroom';
import ImageComponent from './image_component';
import Logo from '../../assets/images/_main_logo.png';

const Header = () => {
    return (
        <HeadRoom>
        <nav className="Main-header navbar navbar-expand-lg">
            <a href="javascript:void(0)" className="navbar-brand logo-font">
                <ImageComponent src={Logo}/>
            </a>
            <div className="navbar-collapse" id="List-right">
                <ul className="navbar-nav ml-auto">
                    <li className="navbar-right nav-item active hide-mob">
                        <a 
                        href="javascript:void(0)"
                        className="nav-item-font sell-btn sell-btn-font">Sell online</a>
                    </li>
                </ul>
            </div>
        </nav>
        </HeadRoom>
    )
}

export default Header;