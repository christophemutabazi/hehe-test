import React, { Component } from 'react';
import Waypoint from 'react-waypoint';

class AnimatedElement extends Component {
    constructor(props) {
        super(props);
        this.state = { animateClass: "waiting" }
        this.addAnimatedClass = this.addAnimatedClass.bind(this);
    }
    addAnimatedClass() {
        this.setState({animateClass: "waiting animated-element"})
    }
    render() {
        return(
            <Waypoint onEnter={this.addAnimatedClass}>
                <div className={this.state.animateClass}>
                    {this.props.children}
                </div>    
            </Waypoint>
        )

    }
}

export default AnimatedElement;