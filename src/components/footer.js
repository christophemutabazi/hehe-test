import React from 'react';
import Logo from '../../assets/images/_main_logo.png';

const Footer = () => {
    return (
        <footer className="Main-footer">
            <h5>
                &copy;2018 DMM.HeHe
            </h5>
        </footer>
    )
}

export default Footer;