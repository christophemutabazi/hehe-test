# Hehe Test

Landing Page implemented

### Tools Used

```
> Node(NPM)
> Reactjs
> SASS
> HTML5
> CSS3
```

### Set up dev environment

Below is how to get started with this repo locally

```
> cd hehe-test
> npm install
> npm run dev
```

### Deployment

To deploy you need to run below commands

```
> npm run postinstall ( will create bundles(js & css))
> npm start ( to start the server )
```
